#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>

/*      Cette fonction permet simplement d'afficher      */
/*      une matrice de rang 2*2                          */

void afficherMatrice(int tab[2][2])     
{    
    for (int i = 0; i < 2; i++)
    {
        for(int j = 0; j < 2; j++)
        {
            printf("|%d|", tab[i][j]);
        }
        printf("\n");
    }

}


/*      Cette fonction va initialiser une matrice avec   */
/*      des valeurs aléatoires, comprises entre 0 et 10  */

void initMatrice(int tab[2][2])
{
    srand(time(NULL));
    for (int i = 0; i < 2; i++)
    {
        for(int j = 0; j < 2; j++)
        {
            
            tab[i][j] = rand()%10;
        }
    }
}


int main()
{
    int matrice [2][2];
    int matriceResult [2][2] = {0};

    initMatrice(matrice);
    printf("La matrice est :\n");
    afficherMatrice(matrice);

    /*  On va faire 4 forks, un pour chaque calcul de case de la matriceResult    */

    pid_t pid1 = fork();        
    pid_t pid2 = fork();
    pid_t pid3 = fork();
    pid_t pid4 = fork(); 

    if(pid1 == 0)
    {
        int result = matrice[0][0] * matrice[0][0] + matrice[0][1] * matrice[1][0];
        exit(result);
    }

    if(pid2 == 0)
    {
        int result = matrice[0][0] * matrice[0][1] + matrice[0][1] * matrice[1][1];
        exit(result);
    }

    if(pid3 == 0)
    {
        int result = matrice[1][0] * matrice[0][0] + matrice[1][1] * matrice[1][0];
        exit(result);
    }

    if(pid4 == 0)
    {
        int result = matrice[1][0] * matrice[0][1] + matrice[1][1] * matrice[1][1];
        exit(result);
    }

    int status;

    /*  --------------------------------------------------------------- */
    /*  On va attendre les résultats des processus fils                 */
    /*                                                                  */
    /*  Pour être certain de récupérer le résultat du bon fils, on va   */
    /*  utiliser waitpid() qui permet de spécifier quel fils on attend  */
    /*                                                                  */
    /*  WEXITSTATUS permet d'obtenir la valeur retourner des fils, sans */
    /*  avoir à manipuler le bit fort/faible (si le fils est kill/exit) */
    /*  --------------------------------------------------------------- */
    
    waitpid(pid1, &status, 0);
    matriceResult[0][0] = WEXITSTATUS(status);

    waitpid(pid2, &status, 0);
    matriceResult[0][1] = WEXITSTATUS(status);

    waitpid(pid3, &status, 0);
    matriceResult[1][0] = WEXITSTATUS(status);

    waitpid(pid4, &status, 0);
    matriceResult[1][1] = WEXITSTATUS(status);

    /*  Il ne reste plus qu'à afficher le résultat  */

    printf("Le résultat est :\n");
    afficherMatrice(matriceResult);

}