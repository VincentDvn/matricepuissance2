# MatricePuissance2

Programme calculant le carré d'une Matrice en utilisant des processus fils. Chaque processus fils va calculer une case de la matrice^2. 
A noter : la matrice est une matrice déterminée aléatoirement et de taille (2, 2).
